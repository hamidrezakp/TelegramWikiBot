#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to print link of wiki pages to telegram conversations
# This program is dedicated to the public domain under the Apache v2 license.
"""
This Bot uses the Updater class to handle the bot.
First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
from uuid import uuid4
import json, requests
import re

from telegram import InlineQueryResultArticle, ParseMode, \
    InputTextMessageContent
from telegram.ext import Updater, InlineQueryHandler, CommandHandler, MessageHandler, Filters
import logging

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    update.message.reply_text('Hi!\nI am a InLine bot, use \"@irwikibot\" in your conversations\n\nCommands:\nadmin --> print admin username and email\nping --> pong')

def commands(bot, update):
    cmd = update.message.text
    if cmd.lower() == 'admin':
        update.message.reply_text('Hamid Rexa Kaveh Pishghadam @hamidrezakp\nHamidreza[at]hamidrezakp.ir')
    elif cmd.lower() == 'ping':
        update.message.reply_text('Pong')
    elif cmd.lower() == 'pong':
        update.message.reply_text('Ping :)')

def escape_markdown(text):
    """Helper function to escape telegram markup symbols"""
    escape_chars = '\*_`\['
    return re.sub(r'([%s])' % escape_chars, r'\\\1', text)


def inlinequery(bot, update):
    query = update.inline_query.query
    if query == '' :
        return

    results = list()
    search_results = search(query)
    print(search_results)
    
    if len(search_results[1]) == 0 :
        return
    
    for i in range(0, len(search_results[1])):
        results.append(InlineQueryResultArticle(id=uuid4(),
                                                title=search_results[1][i],
                                                input_message_content=InputTextMessageContent(message_text=search_results[3][i]),
                                                thumb_url='http://forum.ubuntu.ir/Themes/ubuntu-ir-theme/images/on.png'
                                                ))

    update.inline_query.answer(results)

def search(word):
    api_url = 'https://wiki.ubuntu.ir/api.php?action=opensearch&search=' + word + '&format=json'
    response = requests.get(url=api_url)
    data = json.loads(response.text)
    
    return data
    
    

def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"' % (update, error))


def main():
    # Create the Updater and pass it your bot's token.
    updater = Updater("Token")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(MessageHandler(Filters.text, commands))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(InlineQueryHandler(inlinequery))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Block until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
